import os

def main():
  lr = [0.001, 0.01 , 0.05, 0.1, 0.15, 0.2, 0.25, 0.3 ,0.5 ]
  opt = ["sgd","adam", "momentum","RMSProp","adagrad"]
  lr_mode=[6,3,4,5]
  batchSize=[32,64,128,256,512]
  for batch in batchSize:
    for op in opt :
      for lr_m in lr_mode:
        for l in lr:
          os.system('python3 train.py -aug_list "[batchSize]" -aug_prob "[{batch}]" -opt {op} -lr_mode {lr_m} -a0 {l}'.format(batch=str(batch), op=op,lr_m=str(lr_m), l=str(l)))


if __name__ == "__main__":
    main()
